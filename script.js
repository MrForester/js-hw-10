
const tabs = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelectorAll('.tabs-content li');

tabs.forEach((tab) => {
  tab.addEventListener('click', () => {
    
    const tabId = tab.dataset.tabs;
    
    tabsContent.forEach((tabContent) => {
      tabContent.classList.add('no-text');
    });
   
    const selectedTabContent = document.querySelector(`.tabs-content li:nth-child(${tabId})`);
    selectedTabContent.classList.remove('no-text');
    
    tabs.forEach((tab) => {
      tab.classList.remove('active');
    });
    tab.classList.add('active');
  });
});